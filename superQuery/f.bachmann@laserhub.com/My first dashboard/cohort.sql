with cohort_items as (
  select
    date_trunc(c.account_created_at, month) as cohort_month,
    c.customer_id
  from `lh-data-warehouse-prod.reporting.dim_customer` c
  where account_created_at is not null 
  order by 1, 2
),
user_activities as (
  select
    A.customer_id,
    date_diff(
      date_trunc(date(A.ordered_at), month),
      date(C.cohort_month),month
    ) as month_number
  from `lh-data-warehouse-prod.reporting.fct_quote_line_items` A
  left join cohort_items C ON A.customer_id = C.customer_id
  left join `lh-data-warehouse-prod.reporting.dim_quote` q on A.quote_id=q.quote_id
  where q.checkout_status='Ordered'
  group by 1, 2  
)
,cohort_size as (
  select cohort_month, count(1) as num_users
  from cohort_items
  group by 1
  order by 1
)
,retention_table as (
  select
    C.cohort_month,
    A.month_number,
    count(1) as num_users
  from user_activities A
  left join cohort_items C ON A.customer_id = C.customer_id
  group by 1, 2
)
-- final value: (cohort_month, size, month_number, percentage)
select
  B.cohort_month,
  S.num_users as total_users,
  B.month_number,
  B.num_users * 100 / S.num_users as percentage,
  B.num_users repeat_users
from retention_table B
left join cohort_size S ON B.cohort_month = S.cohort_month
where B.cohort_month IS NOT NULL
order by 1, 3