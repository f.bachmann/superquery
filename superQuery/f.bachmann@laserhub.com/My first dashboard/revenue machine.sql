with companies as (
select 
_airbyte_engagements_hashid
,company_id
FROM `lh-data-raw-prod.hubspot.engagements_associations` a
CROSS JOIN UNNEST(a.companyIds) AS company_id
),
first_quotes as (
 SELECT
      -- company = customer
      company_id

    , MIN(created_at) AS first_quote_created_at
    , COUNT(created_at) AS number_of_quote

  FROM `lh-data-warehouse-prod.base.base_lh_offers` o

  GROUP BY 1
),
customer_lc_stages as (

SELECT
      -- company = customer
     c.hubspot_company_id
    , min(c.account_created_at) as account_created_at
    , min(fq.first_quote_created_at) as first_quote_created_at
    , min(c.first_order_created_at) as first_order_created_at


  FROM `lh-data-warehouse-prod.reporting.dim_customer` c
  left join first_quotes fq on c.customer_id=fq.company_id
  --where  c.hubspot_company_id = "hs-1183005705"
  GROUP BY 1
  order by 2
)


select 
e.*
,TIMESTAMP_MILLIS(e.createdAt) as engagement_created_at
,c.company_id
,case 
    when date(TIMESTAMP_MILLIS(e.createdAt))< date(clcs.account_created_at) then 'Prospect' 
    when date(TIMESTAMP_MILLIS(e.createdAt))>= date(clcs.account_created_at)  
        AND date(TIMESTAMP_MILLIS(e.createdAt)) < date(first_quote_created_at)   then 'Lead' 
    when date(TIMESTAMP_MILLIS(e.createdAt))>= date(clcs.first_quote_created_at)  
        AND date(TIMESTAMP_MILLIS(e.createdAt)) < date(first_order_created_at)   then 'Opportunity' 
    when date(TIMESTAMP_MILLIS(e.createdAt))>= date(clcs.first_order_created_at) THEN 'Customer'      
END Lifcycle 
FROM `lh-data-raw-prod.hubspot.engagements` e
left join companies c on e._airbyte_engagements_hashid=c._airbyte_engagements_hashid
left join customer_lc_stages clcs on CONCAT("hs-",c.company_id)=clcs.hubspot_company_id
    --and date_trunc(date(clcs.account_created_at),month)<=date_trunc(date(TIMESTAMP_MILLIS(e.createdAt)),month)
where c.company_id = "1183005705"
order by engagement_created_at 


